package tuesday18;

public class Dog extends Animal {
    Dog(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return "Hi. I'm a dog, my name is: " + this.name;
    }
}