package tuesday18;

public class Animal {
    String name;

    Animal(String name) {
        this.name = "'" + name.toUpperCase() + "'";
    }

    public String toString() {
        return "Hello, I'm an animal, my name is: " + this.name;
    }
}