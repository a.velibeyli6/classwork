package tuesday18;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Animal dog = new Dog("Hasky");
        Animal cat = new Cat("Vasiliy");
        Animal fish = new Fish("Nemo");
        Animal dragon = new Animal("Drakaris") {
            @Override
            public String toString() {
                return "Hi. I'm a dragon, my name is: " + this.name;
            }
        };
        ArrayList<Animal> animals = new ArrayList<Animal>();
        animals.add(dog);
        animals.add(cat);
        animals.add(dragon);
        animals.add(fish);
        for (Animal a : animals) {
            System.out.println(a);
        }
    }
}