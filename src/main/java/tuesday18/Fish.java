package tuesday18;

public class Fish extends Animal {
    Fish(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return "Hi. I'm a fish, my name is: l" + this.name;
    }
}