package tuesday18;

public class Cat extends Animal {
    Cat(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return "Hi. I'm a cat, my name is: l" + this.name;
    }
}