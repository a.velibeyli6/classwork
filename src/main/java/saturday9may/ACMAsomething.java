package saturday9may;


import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ACMAsomething {
    static class Pair<A, B> {
        A a;
        B b;

        public Pair(A a, B b) {
            this.a = a;
            this.b = b;
        }
    }

    static int[] acmTeam(String[] topics) {
        int len = topics.length;

        List<Pair<Integer, Integer>> permutations = IntStream.range(0, len).boxed().flatMap(a ->
                IntStream.range(a + 1, len).boxed().map(b -> new Pair<>(a, b)))
                .collect(Collectors.toList());

        List<Integer> data = permutations.stream().map(
                pair -> new Pair<>(pair, doIt(topics, pair.a, pair.b).size()))
                .filter(p -> p.b > 0)
                .map(p -> p.b)
                .collect(Collectors.toList());

        int maxT = data.stream().max(Comparator.comparingInt(a -> a)).orElseThrow(RuntimeException::new);
        int count = (int) data.stream().filter(n -> n == maxT).count();
        return new int[]{maxT, count};


    }

    private static List<Integer> doIt(String[] topics, int a, int b) {
        return IntStream.range(0, topics[0].length()).boxed().map(x ->
                new Pair<>(x, topics[a].charAt(x) == '1' || topics[b].charAt(x) == '1'))
                .filter(p -> p.b)
                .map(p -> p.a)
                .collect(Collectors.toList());
    }
}
