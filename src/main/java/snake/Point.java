package snake;


public class Point {
    public int x;
    public int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public static Point of(int x, int y) {
        return new Point(x, y);
    }

    public Point move(Point point) {
        return Point.of(x + point.x, y + point.y);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point point = (Point) o;
        return x == point.x &&
                y == point.y;
    }

//    @Override
//    public int hashCode() {
//        return Objects.hash(x, y);
//    }

    @Override
    public int hashCode() {
        return x << 16 + y;
    }

    @Override
    public String toString() {
        return String.format("[%2d:%2d]", x, y);
    }
}
