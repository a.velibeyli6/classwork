package snake;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Lee {
    private static final int OBSTACLE = -9;
    private static final int START = -1;
    private static final int EMPTY = 0;
    public final List<Point> deltas = Arrays.asList(
            Point.of(0, -1), Point.of(-1, 0),
            Point.of(0, 1), Point.of(1, 0)
    );
    public static int[][] board;
    public final int width;
    public final int height;

    public Lee(int width, int height) {
        this.width = width;
        this.height = height;
        board = new int[width][height];
    }

    public void set(Point point, int val) {
        board[point.x][point.y] = val;
    }

    public int get(Point point) {
        return board[point.x][point.y];
    }

    public Stream<Point> neighbors(Point point) {
        return deltas.stream().
                map(p -> p.move(point))
                .filter(this::isOnBoard);
    }

    private boolean isOnBoard(Point point) {
        return point.x >= 0 && point.x < width && point.y >= 0 && point.y < height;
    }

    public Set<Point> neighborsUnvisited(Point point) {
        return neighbors(point)
//                .filter(p -> get(p) == EMPTY)
                .filter(this::isUnvisited)
                .collect(Collectors.toSet());
    }

    private boolean isUnvisited(Point point) {
        return get(point) == EMPTY;
    }

    public List<Point> neighborsByValue(Point point, int val) {
        return neighbors(point)
                .filter(p -> get(p) == val)
                .collect(Collectors.toList());
    }

    public void initializeBoard(List<Point> obstacles) {
        IntStream.range(0, height).boxed().flatMap(y ->
                IntStream.range(0, width)
                        .mapToObj(x -> Point.of(x, y))
        ).forEach(p -> set(p, EMPTY));

        obstacles.forEach(p -> set(p, OBSTACLE));
    }

    public Optional<List<Point>> trace(Point start, Point finish, List<Point> obstacles) {
        initializeBoard(obstacles);
        int[] count = {0};
        set(start, START);
        Set<Point> curr = new HashSet<>();
        curr.add(start);
        boolean found = false;

        while (!(found || curr.isEmpty())) {
            count[0]++;
            Set<Point> next = curr.stream()
                    .map(p -> neighborsUnvisited(p))
                    .flatMap(Collection::stream)
                    .collect(Collectors.toSet());
            next.forEach(p -> set(p, count[0]));
            printMe(Collections.emptyList());
            found = next.contains(finish);
            curr.clear();
            curr.addAll(next);
        }
        if (!found) return Optional.empty();

        LinkedList<Point> path = new LinkedList<>();
        Point fin = finish;
        path.add(finish);
        set(start, 0);
        while (count[0] > 0) {
            count[0]--;
            Point next = neighborsByValue(fin, count[0]).get(0);
            fin = next;
            path.addFirst(next);
        }
        printMe(path);
        return Optional.of(path);
    }

    private String formatted(Point point, List<Point> path) {
        int val = get(point);
        if (val == OBSTACLE) return " XX";
        if (path.isEmpty()) return String.format("%3d", val);
        if (path.contains(point)) return String.format("%3d", val);
        return " --";
    }

    public void printMe(List<Point> path) {
        String s = IntStream.range(0, height).mapToObj(y ->
                IntStream.range(0, width).mapToObj(x ->
                        Point.of(x, y)
                ).map(p -> formatted(p, path))
                        .collect(Collectors.joining())
        ).collect(Collectors.joining("\n"));
        System.out.printf("%s\n\n", s);
    }


}
