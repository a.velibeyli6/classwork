package monday27april;

import java.util.stream.IntStream;

public class ApplesOranges {
    public static void main(String[] args) {

    }

    static void countApplesAndOranges(int s, int t, int a, int b, int[] apples, int[] oranges) {


        System.out.println(IntStream.range(0, apples.length).boxed().filter(x -> (apples[x] + a >= s && apples[x] + a <= t)).count());
        System.out.println(IntStream.range(0, oranges.length).boxed().filter(x -> (oranges[x] + b >= s && oranges[x] + b <= t)).count());

    }
}
