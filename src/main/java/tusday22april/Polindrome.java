package tusday22april;

import java.util.Scanner;

public class Polindrome {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String A = sc.next();
        /* Enter your code here. Print output to STDOUT. */
        boolean isPolindrome = true;
        char[] chars = A.toCharArray();
        for (int i = 0; i < chars.length / 2; i++) {
            if (chars[i] != chars[chars.length - 1 - i]) {
                isPolindrome = false;
                break;
            }
        }
        if (isPolindrome) System.out.println("Yes");
        else System.out.println("No");
    }
}
