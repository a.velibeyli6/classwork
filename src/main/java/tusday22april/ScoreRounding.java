package tusday22april;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class ScoreRounding {
    public static void main(String[] args) {
        List<Integer> grades = new ArrayList<>();
        grades.add(80);
        grades.add(96);
        grades.add(18);
        grades.add(73);
        grades.add(78);
        grades.add(60);
        grades.add(60);
        grades.add(15);
        grades.add(45);
        grades.add(15);
        grades.add(10);
        grades.add(5);
        grades.add(46);
        grades.add(87);
        grades.add(33);
        grades.add(60);
        grades.add(14);
        grades.add(71);
        grades.add(65);
        System.out.println("original: " + grades);
        System.out.println("rounded: " + gradingStudents(grades));

    }

    public static List<Integer> gradingStudents(List<Integer> grades) {
        // Write your code here


        for (int i = 0; i < grades.size(); i++) {
            if (grades.get(i) >= 38) {
                grades.set(i, check(grades.get(i)));
            }
        }
        return grades;
    }

    private static int check(int a) {
        return a % 5 >= 3 ? (a + (5 - a % 5)) : a;
    }
}
