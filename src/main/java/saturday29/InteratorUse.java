package saturday29;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class InteratorUse implements Iterable<String> {
    private final String[] months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    private int count = 0;

    @Override
    public Iterator<String> iterator() {
        return new Iterator<String>() {
            List<Integer> indexes = new ArrayList<>();

            @Override
            public boolean hasNext() {
                return count < months.length;
            }

            @Override
            public String next() {
                int a = (int) Math.random() * months.length;

                while (!unique(indexes, a)) {
                    a = (int) Math.random() * months.length;
                }

                indexes.add(a);
                count++;
                return months[a];
            }
        };
    }

    private boolean unique(List<Integer> indexes, int a) {
        for (int index : indexes) {
            if (index == a) return false;
        }
        return true;
    }
}
