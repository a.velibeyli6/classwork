package tuesday11;

public class FunctionApp {
    int add(int a,int b,int c){
        return a+b+c;
    }
    int add(int a,int b,int c,int d){
//        return add(add(a,b,c),d);
        return a+b+c+d;
    }
    int add1(int... ints){  // or (int[] ints)  // DOTS WILL BE LIKE AN ARRRAY//  INTS JUST A NAME /CAN NAME WHATEVER YOU WANT
//        ints[0];  CAN BE TREATED AS ARRAY
//        ints[1];
        int total=0;
        for(int i:ints){
            total+=i;
        }
        return total;
    }
    int add2(int[] vals){
        int total=0;
        for(int i:vals){
            total+=i;
        }
        return total;
    }
    void implement(){
        add1( 1,2,3,4,5,6);
//        add2(1,2,3,4,5,6);// cannot be done with ARRAY type
        add2(new int[]{1,2,3,4,5,6});

    }
//    String combine(String... stringoos){
//        throw new IllegalArgumentException("not impl");
//    }

    int add(int... ints){return 0;}
}