package thursday13;

public class SmartBox<A> {
    private A value;
    public void setValue(A value){
        this.value=value;
    }
    public A getValue(){
        return this.value;
    }
}
