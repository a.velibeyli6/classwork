package thursday13;

public class ClassWrappers {
    public static void main(String[] args) {
        int i=5;
        Integer j=6;
        i=j;
        j=i;
        System.out.println(i+"     "+j);

        new SmartBox<Integer>();  // That is why they created Interger and blah blah blah
        new SmartBox<Pizza>();
        new SmartBox<String>();


    }
}
