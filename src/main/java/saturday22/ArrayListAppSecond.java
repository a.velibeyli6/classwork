package saturday22;

import java.util.*;

public class ArrayListAppSecond {
    public static void main(String[] args) {
        Random random = new Random();
        String[] names = {"Abigail","Brianna","Olivia","Emma","Megan","Grace","Victoria","Rachel","Anna","Sydney","Destiny","Morgan","Jennifer","Jasmine","Haley","Julia","Kaitlyn","Nicole","Amanda","Katherine"};
        List<String> original = new ArrayList<>();
        for(int i=0;i<30;i++){
            original.add(names[random()]);
        }
        Set<String> unique= new HashSet<>();
        original.forEach(el->unique.add(el));
        original.forEach(el-> System.out.println(el));
        System.out.println(unique);


    }

    private static int random() {
        return (int)(Math.random()*20);
    }
}
