package saturday22;

import java.util.*;

public class ArrayListApp {
    public static void main(String[] args) {
        Random random = new Random();
        List<Integer> original = new ArrayList<>();
        for(int i=0;i<30;i++){
            original.add(random());
        }
        Set<Integer> unique= new HashSet<>();
//        for(int i=0;i<30;i++){
//            unique.add(original.get(i));
//         }
        original.forEach(el->unique.add(el));
        original.forEach(el-> System.out.println(el));
        System.out.println(unique);
        List<Integer> uniqueAsList = new ArrayList<>();
        uniqueAsList.addAll(unique);
//        System.out.println(uniqueAsList.sort());

    }

    private static int random() {
        return (int)(Math.random()*20 -10);
    }
}
