package sunday26april;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class BetweenTwoSets {

    public static void main(String[] args) {
        List<Integer> a = new ArrayList<>();
        a.add(2);
        a.add(4);

        List<Integer> b = new ArrayList<>();
        b.add(16);
        b.add(32);
        b.add(96);

        System.out.println(getTotalX(a, b));

    }

    public static int getTotalX(List<Integer> a, List<Integer> b) {
        // Write your code here
//        List<Integer> bs = ints(b);
//        AtomicInteger count = new AtomicInteger();
//        for (int i = 0; i < bs.size(); i++) {
//            int finalI = i;
//            if (a.stream().filter(x -> bs.get(finalI) % x == 0).count() == a.size()) count.getAndIncrement();
//        }
//        return count.get();


//        return (int) ints(b).stream().filter(x -> (a.stream().filter(y -> x % y == 0).count() == a.size())).count();
        return (int) ints(b).stream().filter(x -> (isOkey(a, x, false))).count();
    }

    private static List<Integer> ints(List<Integer> list) {
        int min = list.stream().min((b1, b2) -> b1 - b2).orElse(0);
//        List<Integer> divisible = new ArrayList<>();
//        for (int i = 1; i <= min; i++) {
//            if (isOkey(list, i)) {
//                divisible.add(i);
//            }
//        }
        return IntStream.rangeClosed(1, min).filter(x -> isOkey(list, x, true)).boxed().collect(Collectors.toList());
    }

    private static boolean isOkey(List<Integer> list, int i, boolean commonDivision) {

        return commonDivision ? ((int) list.stream().filter(x -> x % i == 0).count() == list.size()) :
                ((int) list.stream().filter(x -> i % x == 0).count() == list.size());
    }

}
