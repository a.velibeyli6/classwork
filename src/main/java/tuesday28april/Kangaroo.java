package tuesday28april;

import java.util.stream.IntStream;

public class Kangaroo {
    public static String kangaroo(int x1, int v1, int x2, int v2) {

        if (v1 > v2) {
            int count = 1;
            while ((x1 + v1 * count) <= (x2 + v1 * count)) {
                if (x1 + v1 * count == x2 + v2 * count)
                    return "YES";
                count++;
            }
        }
        return "NO";
    }

    public static void main(String[] args) {
        System.out.println(kangaroo(0, 3, 4, 2));
        System.out.println(kangaroo(0, 2, 5, 3));
    }
}
