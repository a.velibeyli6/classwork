package tuesday28april;

public class BestScores {
    static int[] breakingRecords(int[] scores) {
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        int cmin = -1;
        int cmax = -1;
        for (int i = 0; i < scores.length; i++) {
            if (scores[i] > max) {
                max = scores[i];
                cmax++;
            }
            if (scores[i] < min) {
                min = scores[i];
                cmin++;
            }
        }

        return new int[]{cmax, cmin};
    }

    public static void main(String[] args) {

    }
}
