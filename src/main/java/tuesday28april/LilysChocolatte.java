package tuesday28april;

import java.util.List;
import java.util.stream.IntStream;

public class LilysChocolatte {
    public static void main(String[] args) {

    }

    static int birthday(List<Integer> s, int d, int m) {
//        int c = 0;
//        for (int i = 0; i < s.size() - (m - 1); i++) {
//            if (IntStream.range(i, i + m)
//                    .boxed()
//                    .mapToInt(s::get).sum() == d) c++;
//        }
//        return c;
        return (int) IntStream.range(0, s.size() - m + 1).filter(x ->
                IntStream.range(x, x + m)
                        .boxed()
                        .mapToInt(s::get).sum() == d).count();
    }
}
