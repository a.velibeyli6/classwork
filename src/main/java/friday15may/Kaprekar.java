package friday15may;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Kaprekar {
    public static void main(String[] args) {
        kaprekarNumbers(1, 100);
    }

    //    static void kaprekarNumbers(int p, int q) {
//        List<Integer> collected = IntStream.range(p, q).boxed().filter(Kaprekar::isKaprekar).collect(Collectors.toList());
//        if (collected.size() == 0) {
//            System.out.println("INVALID RANGE");
//        }else
//        collected.forEach(System.out::println);
//
//    }
//
//    private static boolean isKaprekar(Integer a) {
//        int sq = (int) Math.pow((int) a, 2);
//        String original = String.valueOf(a);
//        int lenor = original.length();
//        String square = String.valueOf(sq);
//        int r = Integer.parseInt(String.valueOf(square.subSequence(square.length() - lenor, square.length())));
//        int l = Integer.parseInt(String.valueOf(square.subSequence(0, square.length() - lenor)));
//        return r + l == a;
//    }
    static class Pair<A, B> {
        A a;
        B b;

        public Pair(A a, B b) {
            this.a = a;
            this.b = b;
        }
    }

    static String itos(long a) {
        return Long.toString(a);
    }

    static long stoi(String s) {
        return s.isEmpty() ? 0 : Long.parseLong(s);
    }

    static int len(long s) {
        return itos(s).length();
    }

    static long sq(long a) {
        return a * a;
    }

    static Pair<String, String> split(long a, int at) {
        long sq = sq(a);
        String sa = itos(a);
        String ns = itos(sq);
        return new Pair<>(
                ns.substring(0, ns.length() - at),
                ns.substring(ns.length() - at)
        );
    }

    static Pair<Long, Long> ptol(Pair<String, String> pair) {
        return new Pair<>(stoi(pair.a), stoi(pair.b));
    }

    static void kaprekarNumbers(int p, int q) {

        String result = IntStream.rangeClosed(p, q).mapToObj(
                n -> new Pair<>(n, ptol(split(n, len(n))))
        ).filter(pair -> pair.a == pair.b.a + pair.b.b)
                .map(pair -> pair.a)
                .map(n -> itos(n))
                .collect(Collectors.joining(" "));

        System.out.println(result.isEmpty() ? "INVALID RANGE" : result);


    }

}
