package monday11may;

import java.util.Arrays;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Encryption {
    public static void main(String[] args) {
        String have_a_nice_day = encryption("feedthedog");
        System.out.println(have_a_nice_day);
    }

    static String encryption(String s) {

        final String s1 = s.replace(" ", "");
        int len = s1.length();
        double l = Math.sqrt(len);
        int l1 = (int) Math.floor(l);
        int l2 = (int) Math.ceil(l);
        int col = l2;
        int row = l1 == l2 ? l1 : l1 * l2 < len ? l2 : l1;

//        String s1 = s.replace(" ", "");
//
//        int len = s1.length();
//        int row = (int) Math.sqrt(len);
//        int col = row + 1;
//        if (row * col < len) row++;
//        char[][] chars = new char[row][col];
//        int c = 0;
//        for (int i = 0; i < row; i++) {
//            for (int j = 0; j < col; j++) {
//                if (c >= len) break;
//                chars[i][j] = s1.charAt(c);
//                c++;
//            }
//        }
//        StringBuilder a = new StringBuilder();
//        for (int j = 0; j < row; j++) {
//            for (int i = 0; i < col; i++) {
//                if (c >= len) break;
//                a.append(s1.charAt(c));
//                c++;
//            }
//            a.append(" ");
//        }

        return IntStream.range(0, col).boxed().flatMap(aa ->
                IntStream.range(0, row).boxed().map(b -> {
                            int idx = b * col + aa;
                            String ss = idx < len ? String.valueOf(s1.charAt(idx)) : "";
                            return b == row - 1 ? ss + " " : ss;
                        }

                )
        ).collect(Collectors.joining());

//        return a.toString().trim();
    }
}
