package wednesday18march;


import java.util.Arrays;
import java.util.Random;


public class Main {
    public static void merge(int[] data, int left, int mid, int right) {

    }

    public static void sort(int[] data, int left, int right) {
        if (left < right) {
            int mid = (left + right) / 2;
            sort(data, left, mid);
            sort(data, mid + 1, right);
        }
    }

    public static void main(String[] args) {
        int[] ints = new Random().ints(1, 50).limit(4).toArray();
        System.out.println(Arrays.toString(ints));

        sort(ints, 0, ints.length - 1);

    }
}
