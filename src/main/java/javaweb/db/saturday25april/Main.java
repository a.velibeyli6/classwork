package javaweb.db.saturday25april;

import java.sql.*;

public class Main {
    private static final String URL = "jdbc:postgresql://localhost:5432/postgres";
    private static final String NAME = "postgres";
    private static final String PASSWORD = "secret";

    public static void main(String[] args) throws SQLException {
        Connection connection = DriverManager.getConnection(URL, NAME, PASSWORD);
//        String SQL = "SELECT * FROM users";
//        PreparedStatement preparedStatement = connection.prepareStatement(SQL);
//        ResultSet resultSet = preparedStatement.executeQuery();
//        while (resultSet.next()) {
//            int id = resultSet.getInt("id");
//            int age = resultSet.getInt("age");
//            String name = resultSet.getString("name");
//            String psw = resultSet.getString("password");
//            System.out.println(String.format("%-10s : %-3d : %-2d : %-10s", name, id, age, psw));
//
//        }
//
        String sql2 = "select * from users where name = ?";
        PreparedStatement ps2 = connection.prepareStatement(sql2);
        ps2.setString(1, "David");
        ResultSet rs2 = ps2.executeQuery();
//
//        while (rs2.next()) {
//            int id = rs2.getInt("id");
//            int age = rs2.getInt("age");
//            String name = rs2.getString("name");
//            String psw = rs2.getString("password");
//            System.out.println(String.format("%-10s : %-3d : %-2d : %-10s", name, id, age, psw));
//
//        }

        String sql3 = "select id from users where name='David'";
        ResultSet r3 = connection.prepareStatement(sql3).executeQuery();
        while(r3.next()){
            System.out.println(r3.getString("name"));
        }


    }
}
