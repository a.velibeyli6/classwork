create table if not exists users
(
	id serial not null
		constraint users_pk
			primary key,
	name varchar,
	age integer,
	password varchar
);

alter table users owner to postgres;

create unique index if not exists users_id_uindex
	on users (id);

-- 	////////////////////////////////////////////////


   create table if not exists groups
(
	id serial not null
		constraint groups_pk
			primary key,
	name varchar
);

alter table groups owner to postgres;

create unique index if not exists groups_id_uindex
	on groups (id);



