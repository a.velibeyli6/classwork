update users set password = 'ABC123' where password is null
--  Then we can make our password column NOT NULL
alter table users alter column password set not null -- SET || DROP - is opposite to SET
-- Next is about joining two tables
select * from users s
join groups g on s."group"=g.id

select * from users u
left outer join groups g on u.group = id

select u.id, u.name,g.id,g.name from users u
full outer join groups g on u.group = g.id


-- // Creating foreign key
alter table "joiningAB"
          	add constraint joiningab___fk2
          		foreign key (bid) references books