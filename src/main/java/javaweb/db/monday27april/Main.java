package javaweb.db.monday27april;

import java.sql.*;

public class Main {
    private static final String URL = "jdbc:postgresql://localhost:5432/postgres";
    private static final String NAME = "postgres";
    private static final String PASSWORD = "secret";

    public static void main(String[] args) throws SQLException {
        Connection conn = DriverManager.getConnection(URL, NAME, PASSWORD);
        String sql = "delete from public.users where id = 8";
        //        String sql = "insert into users (id,name,age,password) values (default,?,?,?)";
        PreparedStatement pr = conn.prepareStatement(sql);
//        pr.setString(1, "fja;fj");
//        pr.setString(3, "1111111111111111111111111");
//        pr.setInt(2, 2);
        pr.executeUpdate();
//        while (r.next()) {
//            System.out.println(String.format("%d : %s : %d : %s",
//                    r.getInt("id"),
//                    r.getString("name"),
//                    r.getInt("age"),
//                    r.getString("password")
//            ));
//        }
    }
}
