package javaweb.db.thursday23april;

import java.sql.*;

public class Main {
    public static final String URL = "jdbc:postgresql://localhost:5432/postgres";
    public static final String NAME = "postgres";
    public static final String PASSWORD = "secret";

    public static void main(String[] args) throws SQLException {
        Connection connection = DriverManager.getConnection(URL, NAME, PASSWORD);
        String SQL = "SELECT * FROM users";
        PreparedStatement preparedStatement = connection.prepareStatement(SQL);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            int age = resultSet.getInt("age");
            String name = resultSet.getString("name");
            System.out.println(String.format("%-10s : %-2d : %-3d", name, age, id));
        }

    }
}
