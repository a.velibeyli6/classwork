package javaweb.rest.server;

import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import java.net.URI;

public class ServletApp {
    public static void main(String[] args) {
        ResourceConfig config = new ResourceConfig().packages("javaweb/rest/server/resources");
        GrizzlyHttpServerFactory.createHttpServer(
                URI.create("http://localhost:8081/"),
                config
        );
    }
}
