package javaweb.rest.server.resources;

import javaweb.rest.entities.Book;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;

@Path("/books")
public class RsBooks {

    Map<Integer, Book> books = new HashMap<Integer, Book>() {{
        put(1, new Book(1, "Java", "Alex"));
        put(2, new Book(2, "Scala", "Alexandro"));
        put(3, new Book(3, "Python", "Alexey"));
    }};

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response bookAll() {
        return Response
                .status(200)
                .entity(books.values())
                .build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Response bookOne(@PathParam("id") int bookID) {
        return Response.status(200).entity(books.get(bookID)).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addOne(Book book) {
        System.out.println("book is :" + book);
        return Response.
                status(201)
                .build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("form")
    public Response forming(@QueryParam("a") String a, @QueryParam("b") String b) {
        System.out.println("a is :" + a);
        System.out.println("b is :" + b);
        return Response.
                status(201)
                .build();
    }

}
