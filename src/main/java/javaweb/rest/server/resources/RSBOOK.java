package javaweb.rest.server.resources;

import javaweb.rest.entities.Book;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;

@Path("/al")
public class RSBOOK {

    Map<Integer, Book> books = new HashMap<Integer, Book>() {{
        put(1, new Book(1, "Javfdasfa", "Alexafdaf"));
        put(2, new Book(2, "Scafdaafdasala", "Alexaafdandro"));
        put(3, new Book(3, "Pytafdahon", "fdasdfAlexey"));
    }};

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response bookAll() {
        return Response
                .status(200)
                .entity(books.values())
                .build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Response bookOne(@PathParam("id") int bookID) {
        return Response.status(200).entity(books.get(bookID)).build();
    }



}
