package javaweb.rest.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import javaweb.rest.entities.Book;

import javax.ws.rs.HttpMethod;
import javax.ws.rs.core.MediaType;
import java.io.*;
import java.net.*;
import java.util.Optional;
import java.util.stream.Collectors;

public class ClientApp {
    public static void main(String[] args) {

//        getExample();
        postExample();
    }

    private static void postExample() {
        String postRequest = "http://localhost:8081/books";
        mkPost(postRequest);
    }

    private static void mkPost(String postRequest) {
        try {
            URL url = new URL(postRequest);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod(HttpMethod.POST);
            conn.setRequestProperty("Content-type", MediaType.APPLICATION_JSON);
            conn.setDoOutput(true);
            Book book = new Book(5, "The Richest man in Babylon", "Drew Badger");
            ObjectMapper mapper = new ObjectMapper();
            byte[] bis = mapper.writeValueAsBytes(book);
            try (OutputStream os = conn.getOutputStream()) {
                os.write(bis);
            }
            try (InputStream is = conn.getInputStream()) {

            } catch (IOException es) {
                es.printStackTrace();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void getExample() {
        String getRequest = "http://localhost:8081/al/2";
        Optional<String> s = mkGet(getRequest);
        System.out.println(s);
        ObjectMapper mapper = new ObjectMapper();
        Optional<Book> book = s.flatMap(body -> {
            try {
                return Optional.of(mapper.readValue(body, Book.class));
            } catch (JsonProcessingException e) {
                return Optional.empty();
            }
        });
        System.out.println(book);
    }

    private static Optional<String> mkGet(String getRequest) {
        try {
            URL url = new URL(getRequest);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod(HttpMethod.GET);
            try (InputStream is = conn.getInputStream()) {
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                return Optional.of(br.lines().collect(Collectors.joining("\n")));
            } catch (IOException ignored) {
                return Optional.empty();
            }
        } catch (IOException ignored) {
            return Optional.empty();
        }


    }
}
