package javaweb.json.wednesday6may;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

public class ServletConsume extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        BufferedReader reader = req.getReader();
//        Student student = mapper.readValue(reader, Student.class);
        Student[] students = mapper.readValue(reader, Student[].class);
        System.out.println(Arrays.toString(students));
//        System.out.println(student);

        try (PrintWriter w = resp.getWriter()) {
            w.write("got it");
        }
    }
}
