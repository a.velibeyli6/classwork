package javaweb.json.wednesday6may;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

public class ServletProvide extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Student s0 = new Student("Aflatun", 19, "BE4");
        List<Student> students = Arrays.asList(
                new Student("Jack", 19, "BE4"),
                new Student("John", 20, "BE3"),
                new Student("Jimmy", 21, "BE2")
        );
        ObjectMapper mapper =
                new ObjectMapper(); // JSON
//                new XmlMapper(); // XML
        String json = mapper.writeValueAsString(s0);
//        resp.setStatus(404);  // It'll set status as we set.
        try (PrintWriter w = resp.getWriter()) {
            w.write(json);
        }
    }
}
