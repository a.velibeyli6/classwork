package javaweb.json.wednesday6may;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;

public class ServletApp {
    public static void main(String[] args) throws Exception {
        Server server = new Server(8080);
        ServletContextHandler handler = new ServletContextHandler();
        handler.addServlet(ServletProvide.class,"/data/*");
        handler.addServlet(ServletConsume.class,"/consume/*");
        server.setHandler(handler);
        server.start();
        server.join();
    }
}
