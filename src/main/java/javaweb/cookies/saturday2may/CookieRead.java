package javaweb.cookies.saturday2may;

import org.eclipse.jetty.servlet.Source;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.stream.Collectors;

public class CookieRead extends HttpServlet {
    private final String s;

    public CookieRead(String s) {
        this.s = s;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        try (PrintWriter w = resp.getWriter()) {
            w.write("reading the " + s + " slash thing, you know\n");
            String cookies = Arrays.stream(req.getCookies())
                    .map(x -> String.format("name -> %s \t value -> %s", x.getName(), x.getValue()))
                    .collect(Collectors.joining("\n"));
            w.write(cookies);
        } catch (NullPointerException ex) {
            System.out.println("ohh, bad :(");
        }
    }


}
