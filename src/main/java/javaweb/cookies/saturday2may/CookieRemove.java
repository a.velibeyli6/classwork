package javaweb.cookies.saturday2may;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class CookieRemove extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try (PrintWriter pr = resp.getWriter()) {
            pr.write("COokie remove");
            Cookie c = new Cookie("login", "");
            c.setMaxAge(10);
            Cookie c2 = new Cookie("abc","");
            c2.setMaxAge(5);
            resp.addCookie(c);
            resp.addCookie(c2);
        } catch (Exception x) {
            x.printStackTrace();
        }
    }
}
