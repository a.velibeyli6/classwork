package javaweb.cookies.saturday2may;

import org.eclipse.jetty.servlet.Source;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class CookieSet extends HttpServlet {
    private final String s;

    public CookieSet(String s) {
        this.s = s;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try (PrintWriter pr = resp.getWriter()) {
            pr.write("COokie set");
            Cookie c = new Cookie(s, "123456");
//            c.setPath("/");
            resp.addCookie(c);
        } catch (Exception x) {
            throw new RuntimeException("something went wrong");
        }
    }
}
