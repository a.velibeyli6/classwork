package javaweb.cookies.saturday2may;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

public class ServletApp {
    public static void main(String[] args) throws Exception {
        Server server = new Server(8080);
        ServletContextHandler handler = new ServletContextHandler();
        handler.addServlet(new ServletHolder(new CookieSet("login")),"/login/cs/*");
        handler.addServlet(new ServletHolder(new CookieSet("abc")),"/abc/cs/*");

        handler.addServlet(new ServletHolder(new CookieRead("login")),"/login/who/*");
        handler.addServlet(new ServletHolder(new CookieRead("abc")),"/abc/who/*");


        handler.addServlet(new ServletHolder(new CookieRemove()),"/login/cr/*");
        server.setHandler(handler);
        server.start();
        server.join();
    }
}
