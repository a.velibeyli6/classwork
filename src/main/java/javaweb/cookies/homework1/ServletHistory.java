package javaweb.cookies.homework1;


import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;
import java.util.stream.Collectors;

public class ServletHistory extends HttpServlet {
    private List<OperationWithUser> list;

    public ServletHistory(List<OperationWithUser> list) {
        this.list = list;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String goLogin = new BufferedReader(new FileReader(new File("content/homework1/goLogin.html"))).lines().collect(Collectors.joining("\n"));

        StringBuilder print = new StringBuilder();
        if (req.getCookies() != null) {
            Cookie cookie = req.getCookies()[0];
            int count = 0;
            for (OperationWithUser op : list) {
                if (op.getLogin().equals(cookie.getName()) && op.getPassword().equals(cookie.getValue())) {
                    count += 1;
                    print.append(String.format("operation number: %d --> x is %s\t y is %s\t op is %s\n", count, op.getX(), op.getY(), op.getOp()));
                }
            }
        }

        try (PrintWriter w = resp.getWriter()) {
            if (req.getCookies() == null) {
                w.write(goLogin);
            } else {
                w.write(print.toString());
            }
        }
    }
}
