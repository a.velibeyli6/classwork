package javaweb.cookies.homework1;



public class OperationWithUser {
    private final String x;
    private final String y;
    private final String op;
    private final String login;
    private final String password;


    public OperationWithUser(String x, String y, String op, String login, String password) {
        this.x = x;
        this.y = y;
        this.op = op;
        this.login = login;
        this.password = password;
    }

    public String getX() {
        return x;
    }

    public String getY() {
        return y;
    }

    public String getOp() {
        return op;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }


}
