package javaweb.cookies.homework1;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class ServletDoMath extends HttpServlet {
    private static List<OperationWithUser> list = new ArrayList<>();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String collected = new BufferedReader(new FileReader(new File("content/homework1/doMath.html"))).lines().collect(Collectors.joining("\n"));
        String goLogin = new BufferedReader(new FileReader(new File("content/homework1/goLogin.html"))).lines().collect(Collectors.joining("\n"));

        try (PrintWriter w = resp.getWriter()) {
            if (req.getCookies() != null) {
                w.write(collected);
            } else {
                w.write(goLogin);
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String x = req.getParameter("x");
        String y = req.getParameter("y");
        String op = req.getParameter("op");
        int result = Calc.doMath(Integer.parseInt(x), Integer.parseInt(y), op);


        String login = req.getCookies()[0].getName();
        String password = req.getCookies()[0].getValue();

        OperationWithUser operationWithUser = new OperationWithUser(x, y, op, login, password);
        list.add(operationWithUser);

        try (PrintWriter w = resp.getWriter()) {
            w.write(String.valueOf(result));
        }

    }

    public static List<OperationWithUser> getList() {
        return list;
    }
}
