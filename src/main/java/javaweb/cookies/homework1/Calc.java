package javaweb.cookies.homework1;

public class Calc {
    private final int x;
    private final int y;
    private final String op;

    public Calc(int x, int y, String op) {
        this.x = x;
        this.y = y;
        this.op = op;
    }

    public static int doMath(int x, int y, String op) {
        Calc calc = new Calc(x, y, op);
        switch (calc.op) {
            case "add":
                return calc.x + calc.y;
            case "sub":
                return calc.x - calc.y;
            case "mul":
                return calc.x * calc.y;
            case "div":
                return calc.x / calc.y;
        }
        return 0;
    }
}
