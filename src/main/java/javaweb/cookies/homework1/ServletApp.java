package javaweb.cookies.homework1;


import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import java.util.List;

public class ServletApp {
    public static void main(String[] args) {
        Server server = new Server(8080);
        ServletContextHandler handler = new ServletContextHandler();

        handler.addServlet(new ServletHolder(new ServlerLogin()), "/calc/login/*");
        handler.addServlet(new ServletHolder(new ServlerLogout()), "/calc/logout/*");
        handler.addServlet(new ServletHolder(new ServletDoMath()), "/calc/do/*");
        List<OperationWithUser> list = ServletDoMath.getList();
        handler.addServlet(new ServletHolder(new ServletHistory(list)), "/calc/history/*");
        handler.addServlet(new ServletHolder(new ServletRedirect("/calc/do")), "/*");
        server.setHandler(handler);
        try {
            server.start();
            server.join();
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
