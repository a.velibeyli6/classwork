package javaweb.servlet.monday27april;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RedirectServlet extends HttpServlet {
    private final String s;

    public RedirectServlet(String s) {
        this.s = s;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        Path path = Paths.get("/", s);
        resp.sendRedirect(s);
    }
}
