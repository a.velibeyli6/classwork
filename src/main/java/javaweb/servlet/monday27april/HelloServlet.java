package javaweb.servlet.monday27april;


import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class HelloServlet extends HttpServlet {
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String collect = new BufferedReader(new FileReader(new File("content/dynamic1.ftl"))).lines().collect(Collectors.joining("\n"));
        Configuration conf = new Configuration(Configuration.VERSION_2_3_28);
        conf.setDirectoryForTemplateLoading(new File("content"));
        conf.setDefaultEncoding(String.valueOf(StandardCharsets.UTF_8));
        conf.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        conf.setWrapUncheckedExceptions(true);
        conf.setLogTemplateExceptions(false);
        try (PrintWriter w = resp.getWriter()) {
            Map<String, Object> data = new HashMap<>();
            String jim = "Jim";
            data.put("name", jim);
            conf.getTemplate("dynamic1.ftl").process(data,w);
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }
}
