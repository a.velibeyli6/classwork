package javaweb.servlet.monday27april;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

public class ServletApp {
    public static void main(String[] args) throws Exception {
        Server server = new Server(8080);
        ServletContextHandler handler = new ServletContextHandler();
        handler.addServlet(new ServletHolder(new StaticServlet()), "/static/*");
        handler.addServlet(new ServletHolder(new ServletLogin()), "/login/*");
        handler.addServlet(new ServletHolder(new HelloServlet()), "/home/*");
        handler.addServlet(new ServletHolder(new RedirectServlet("/login")), "/*");
        server.setHandler(handler);
        server.start();
        server.join();
    }
}
