package javaweb.servlet.wednesday8april;



import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

public class InfoServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try (PrintWriter writer = resp.getWriter()) {
            Map<String, String[]> parameterMap = req.getParameterMap();
            int a = Integer.parseInt(parameterMap.get("a")[0]);
            int b = Integer.parseInt(parameterMap.get("b")[0]);
            String op = parameterMap.get("op")[0];
            if (op.equals("add")) {
                writer.write("a " + op + " b = " + (a + b));
            } else if (op.equals("sub")) {
                writer.write("a " + op + " b = " + (a - b));
            } else if (op.equals("mul")) {
                writer.write("a " + op + " b = " + (a * b));
            } else if (op.equals("div")) {
                if (b == 0) throw new RuntimeException("Cannot divide by zero");
                writer.write("a " + op + " b = " + (a / b));
            }
        }
    }
}
