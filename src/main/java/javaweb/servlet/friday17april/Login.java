package javaweb.servlet.friday17april;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class Login extends HttpServlet {
    DoLogin login;

    public Login(DoLogin login) {
        this.login = login;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String collected = new BufferedReader(new FileReader(new File("content/login.html"))).lines().collect(Collectors.joining("\n"));

        try (PrintWriter wr = resp.getWriter()) {
            wr.write("The actual login: 'login'  and the password is: 'password'\n\n\n\n\n");
            wr.write(collected);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, String> parameterMap = new HashMap<>();
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        parameterMap.put(login, password);
        try (PrintWriter wr = resp.getWriter()) {
            wr.write(this.login.validate(parameterMap));
        }
    }
}
