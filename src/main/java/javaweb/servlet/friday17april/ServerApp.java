package javaweb.servlet.friday17april;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

public class ServerApp {
    public static void main(String[] args) throws Exception {
        Server server = new Server(8080);
        ServletContextHandler handler = new ServletContextHandler();
        DoLogin login = new DoLogin();
        handler.addServlet(new ServletHolder(new Login(login)), "/login/*");
        server.setHandler(handler);
        server.start();
        server.join();
    }
}
