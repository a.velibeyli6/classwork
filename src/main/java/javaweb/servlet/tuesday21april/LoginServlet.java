package javaweb.servlet.tuesday21april;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.stream.Collectors;

public class LoginServlet extends HttpServlet  {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String collect = new BufferedReader(new FileReader(new File("content/login3.html"))).lines().collect(Collectors.joining("\n"));
        try(PrintWriter wr = resp.getWriter()){
            wr.write(collect);
        }
    }
}
