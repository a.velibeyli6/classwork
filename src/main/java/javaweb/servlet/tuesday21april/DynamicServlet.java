package javaweb.servlet.tuesday21april;

import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;

import java.util.HashMap;

public class DynamicServlet extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Configuration conf = new Configuration(Configuration.VERSION_2_3_28);
        conf.setDirectoryForTemplateLoading(new File("content"));
        conf.setDefaultEncoding(String.valueOf(StandardCharsets.UTF_8));
        conf.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        conf.setLogTemplateExceptions(false);
        conf.setWrapUncheckedExceptions(true);


        try (PrintWriter w = resp.getWriter()) {
            String jim = "Jim";
            HashMap<String, Object> data = new HashMap<>();
            data.put("name", jim);
            conf.getTemplate("dynamic1.ftl").process(data, w);
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }
}
