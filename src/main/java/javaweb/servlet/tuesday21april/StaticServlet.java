package javaweb.servlet.tuesday21april;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class StaticServlet extends HttpServlet {
//    private final String a;
//
//    public StaticServlet(String s) {
//        a = s;
//    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String pathInfo = req.getPathInfo();
//        Path path = Paths.get("content", a, pathInfo);
        Path path = Paths.get("content", pathInfo);

        try (ServletOutputStream os = resp.getOutputStream()) {
            Files.copy(path, os);
        }
    }
}
