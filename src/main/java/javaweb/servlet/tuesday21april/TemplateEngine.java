package javaweb.servlet.tuesday21april;

import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;

public class TemplateEngine {
    private final Configuration config;

    public TemplateEngine(String s) throws IOException {
        this.config = new Configuration(Configuration.VERSION_2_3_28) {{
            setDirectoryForTemplateLoading(new File(s));
            setDefaultEncoding(String.valueOf(StandardCharsets.UTF_8));
            setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
            setLogTemplateExceptions(false);
            setWrapUncheckedExceptions(true);
        }};

    }

    public static TemplateEngine folder(String s) throws IOException {
        return new TemplateEngine(s);
    }

    public void render(String s, HashMap<String, Object> data, HttpServletResponse resp) {
        try (PrintWriter w = resp.getWriter()) {
            config.getTemplate(s).process(data, w);
        } catch (TemplateException | IOException e) {
            throw new RuntimeException("FreeMarker exception bro!?");
        }
    }
}
