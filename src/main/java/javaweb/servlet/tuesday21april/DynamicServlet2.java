package javaweb.servlet.tuesday21april;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

public class DynamicServlet2 extends HttpServlet {
    private final TemplateEngine engine;
    public DynamicServlet2(TemplateEngine engine){
        this.engine = engine;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        String jim = "Jim";
        HashMap<String, Object> data = new HashMap<>();
        data.put("name", jim);
        engine.render("dynamic2.ftl", data, resp);

    }

}
