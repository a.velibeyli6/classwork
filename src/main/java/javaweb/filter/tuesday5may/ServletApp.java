package javaweb.filter.tuesday5may;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import javax.servlet.DispatcherType;
import java.util.EnumSet;

public class ServletApp {
    public static void main(String[] args) throws Exception {
        Server server = new Server(8080);
        ServletContextHandler handler = new ServletContextHandler();
        handler.addServlet(new ServletHolder(new ServletSetCookie()),"/cs/*");
        handler.addFilter(new FilterHolder(new ServletFilter()),"/a", EnumSet.of(DispatcherType.REQUEST));
        handler.addServlet(new ServletHolder(new ServletA()),"/a/*");
        server.setHandler(handler);
        server.start();
        server.join();
    }
}
