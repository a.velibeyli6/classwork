package javaweb.filter.tuesday5may;

import org.eclipse.jetty.servlet.Source;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ServletFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    private boolean isHttp(ServletRequest servletRequest, ServletResponse servletResponse) {
        return servletRequest instanceof HttpServletRequest && servletResponse instanceof HttpServletResponse;
    }

    private boolean isRequestValid(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        if (cookies == null) return false;

        for (Cookie c : cookies) {
            if (c.getName().equals("aaa") && c.getValue().equals("bbb")) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        if (isHttp(servletRequest, servletResponse) && isRequestValid((HttpServletRequest) servletRequest)) {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void destroy() {

    }
}
