package thursday30april;

public class TimeConversion {
    public static void main(String[] args) {
        System.out.println(timeConversion("12:40:22AM"));
    }

    static String timeConversion(String s) {
        /*
         * Write your code here.
         */
        String[] splited = s.split(":");
        String substring = splited[2].substring(2);
        String last = splited[2].substring(0, 2);
        splited[2] = last;
        if (substring.equals("PM")) {
            splited[0] = doIt(Integer.parseInt(splited[0]));
        }
        else{
            if(Integer.parseInt(splited[0])==12)
            splited[0] = "00";
        }
        return splited[0].concat(":").concat(splited[1]).concat(":").concat(splited[2]);

    }

    static String doIt(int a) {
        if (a == 12) {
            return "12";
        }
        return String.valueOf(12 + a);
    }
}
