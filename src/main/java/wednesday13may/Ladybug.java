package wednesday13may;

public class Ladybug {
    private static final String YES = "YES";
    private static final String NO = "NO";

    static String happyLadybugs(String b) {
        if (hasSingles(b)) return NO;

        if (b.contains("_")) return YES;

        if (alreadyOrdered(b)) return YES;

        return NO;
    }

    private static boolean alreadyOrdered(String b) {
        String sq = squezze(b);
        return sq.length() == sq.codePoints().distinct().count();
    }

    private static String squezze(String b) {
        if (b.isEmpty()) return b;
        char c0 = b.charAt(0);
        StringBuilder sb = new StringBuilder();
        sb.append(c0);
        for (int i = 1; i < b.length(); i++) {
            char ci = b.charAt(i);
            if (ci != c0) {
                sb.append(ci);
                c0 = ci;
            }
        }
        return sb.toString();
    }

    private static boolean hasSingles(String b) {
        return b.codePoints().filter(c -> c != '_').distinct().anyMatch(c -> b.codePoints().filter(c1 -> c1 == c).count() == 1);
    }
}
