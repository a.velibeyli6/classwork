package friday24april;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class SubArrays {
    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] a = new int[n];
        for(int i=0;i<n;i++){
            a[i] = in.nextInt();
        }
        System.out.println(doIt(a));

    }

    public static int doIt(int[] array) {
        List<List<Integer>> inside = new ArrayList<>();
        for (int i = 0; i < array.length; i++) {
            int s = 0;
            for (int j = i; j < array.length; j++) {
                s += array[j];
                if (s < 0) {
                    List<Integer> collect = IntStream.rangeClosed(i, j).boxed().map(x -> x).collect(Collectors.toList());
                    inside.add(collect);
                }
            }
        }

//        IntStream.range(0, array.length).boxed().flatMap(x ->
//        int[] sum = {0};
//        IntStream.range(x, array.length).boxed().map(y ->
//                sum[0] += array[y];
//        if (sum[0] < 0) {
//            inside.add(IntStream.rangeClosed(x, y).boxed().collect(Collectors.toList()));
//        }
//            )
//        )


        return inside.size();

    }
}
