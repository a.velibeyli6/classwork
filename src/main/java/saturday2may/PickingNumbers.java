package saturday2may;

import java.util.ArrayList;
import java.util.List;


public class PickingNumbers {
    public static void main(String[] args) {
        ArrayList<Integer> ints = new ArrayList<>();
        ints.add(4);
        ints.add(6);
        ints.add(5);
        ints.add(3);
        ints.add(3);
        ints.add(1);
        System.out.println(pickingNumbers(ints));
    }

    public static int pickingNumbers(List<Integer> a) {
        // Write your code here
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < a.size(); i++) {
            Integer integer = a.get(i);
            List<Integer> f = new ArrayList<>();
            f.add(integer);
            for (int j = 0; j < i; j++) {
//                if (Math.abs(a.get(i) - a.get(j)) <= 1) {
//                    f.add(a.get(j));
//                }
                int finalJ = j;
                if (f.stream().filter(x -> Math.abs(x - a.get(finalJ)) <= 1).count() == f.size()) {
                    f.add(a.get(j));
                }
            }
            System.out.println(f.size());
            if (f.size() > max) {
                max = f.size();
            }
        }
        return max;
    }
}
