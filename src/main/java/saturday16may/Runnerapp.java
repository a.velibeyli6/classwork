package saturday16may;

import java.sql.SQLException;
import java.util.List;

public class Runnerapp {
    public static void main(String[] args) throws SQLException {
        TRYtorun trYtorun = new TRYtorun();
        List<User> all = trYtorun.getAll();
        for (User u : all) {
            System.out.printf("name: %s : id: %d\n", u.name, u.id);
        }

        trYtorun.delete();

        List<User> all2 = trYtorun.getAll();
        for (User u : all2) {
            System.out.printf("name: %s : id: %d", u.name, u.id);
        }
    }
}
