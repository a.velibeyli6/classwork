package saturday16may;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TRYtorun {
    public List<User> getAll() throws SQLException {
        String s = "select * from users";
        ResultSet doit = DBApp.doit();
        List<User> users = new ArrayList<>();
        while (doit.next()){
            users.add(new User(doit.getInt("id"),doit.getString("name")));
        }
        return users;
    }

    public void delete() throws SQLException {
        DBApp.delete();
    }
}
