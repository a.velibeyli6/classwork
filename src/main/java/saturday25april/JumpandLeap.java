package saturday25april;

public class JumpandLeap {

//    public static boolean canWin2(int leap, int[] game) {
//        boolean candoit = false;
//        // Return true if you can win the game; otherwise, return false.
//        int curr_pos = 0;
//        for (int i = 1; i <= game.length; i++) {
//
//            if (curr_pos + leap >= game.length || (curr_pos + 1 >= game.length)) {
//                candoit = true;
//                return candoit;
//            }
//
//            if (curr_pos + leap < game.length) {
//                if (game[curr_pos + leap] == 0) {
//                    curr_pos += (leap);
//                }
//            } else if (curr_pos + 1 < game.length) {
//                if (game[curr_pos + 1] == 0) {
//                    curr_pos++;
//                }
//            } else if ((curr_pos - 1) >= 0) {
//                if (game[curr_pos - 1] == 0) {
//                    curr_pos--;
//                    i--;
//                }
//
//            }
//
//        }
//
//        curr_pos = 0;
//        for (int i = 1; i <= game.length; i++) {
//
//            if (curr_pos + leap >= game.length || (curr_pos + 1 >= game.length)) {
//                candoit = true;
//                return candoit;
//            }
//
//            if (curr_pos + 1 < game.length) {
//                if (game[curr_pos + 1] == 0) {
//                    curr_pos += (1);
//                }
//            } else if (curr_pos + leap < game.length) {
//                if (game[curr_pos + leap] == 0) {
//                    curr_pos += leap;
//                }
//            } else {
//                if ((curr_pos - 1) >= 0) {
//                    if (game[curr_pos - 1] == 0) {
//                        curr_pos--;
//                        i--;
//                    }
//
//                }
//            }
//
//        }
//
//        return candoit;
//    }

    public static boolean canWin2(int leap, int[] game) {
        int size = game.length;
        boolean r = true;
        boolean j = true;
        int cur = 0;

        for (int i = 0; i < size; i++) {
            if (cur + leap >= size || cur + 1 >= size) {
                return true;
            }

            if (cur + 1 < size && r) {
                if (game[cur + 1] == 0) {
                    cur++;
                    j = true;
                } else r = false;
            }

            if (cur + leap < size && j) {
                if (game[cur + leap] == 0) {
                    cur += leap;
                    r = true;
                } else j = false;

            }

            if (cur - 1 >= 0 && !r) {
                if (game[cur - 1] == 0) {
                    cur--;
                    j = true;
                }
            }

            if (cur - leap >= 0 && !j) {
                if (game[cur - leap] == 0) {
                    cur -= leap;
                    r = true;
                }
            }
        }

        return false;
    }

    public static boolean canWin(int leap, int[] game) {
        return isSolvable(leap, game, 0);
    }

    private static boolean isSolvable(int leap, int[] game, int idx) {
        // Base Cases
        if (idx >= game.length) {
            return true;
        } else if (idx < 0 || game[idx] == 1) {
            return false;
        }

        game[idx] = 1; // marks as visited

        // Recursive Cases
        return isSolvable(leap, game, idx + leap) ||
                isSolvable(leap, game, idx + 1) ||
                isSolvable(leap, game, idx - 1);
    }

    public static void main(String[] args) {
//        Scanner scan = new Scanner(System.in);
//        int q = scan.nextInt();
//        while (q-- > 0) {
//            int n = scan.nextInt();
//            int leap = scan.nextInt();
//
//            int[] game = new int[n];
//            for (int i = 0; i < n; i++) {
//                game[i] = scan.nextInt();
//            }

        String s = "0 0 0 0 1 0 1 1 1 0 1 1 1";
        String[] s1 = s.split(" ");
        int[] a = new int[s1.length];
        int c = 0;
        for (String i : s1) {
            a[c] = Integer.parseInt(i);
            c++;
        }

        System.out.println((canWin(6, a)) ? "YES" : "NO");
//    }
//        scan.close();
    }
}