package saturday25april;

public class ForIfElse {
    public static void main(String[] args) {
        for (int i = 0; i <= 100; i++) {
            if (i % 2 == 0) {
                System.out.println(i + " is divisible by 2");
            } else if (i % 3 == 0) {
                System.out.println(i + " is divisible by 3");
            } else {
                System.out.println(i + " is not");
            }
        }
    }
}
