package saturday15;
//
//import java.util.Arrays;
//
//public class Shift {
//    public static void main(String[] args) {
//        //DECLARING                //////////////////////
//        StringBuilder print=new StringBuilder();
//        int SIZE=10;
//        int[] array=new int[SIZE];
//        //FILLING              ///////////////////////////
//        int n=0,p=0;
//        int random;
//        for(int i=0;i<SIZE;i++){
//            random=(int)((Math.random()*200)-100);
//            array[i]=random;
//            if(random>0){
//                p++;
//            }else if(random<0){
//                n++;
//            }
//        }
//        //2D ARRAY putting everything in the right place//////////////////////////////////////
//        int position=0;
//        int[][] neg=new int[n][2];   // 2D array .  first element is the index, second will be the element itself;
//        int[][] pos=new int[p][2];
//        for(int i=0;i<SIZE;i++){
//            if(array[i]>0){
//                position=pos.length-p--;
//                pos[position][0]=i;
//                pos[position][1]=array[i];
//            }else if(array[i]<0){
//                position=neg.length-n--;
//                neg[position][0]=i;
//                neg[position][1]=array[i];
//            }
//        }
//        System.out.println(Arrays.toString(array));
//        System.out.println(Arrays.deepToString(pos));
//        System.out.println(Arrays.deepToString(neg));
////        for(int i=0;i<SIZE;i++){
////            if(array[i]>0){
////                if((pos.length-p--)+1>pos.length){
////                    newArray[pos[0][0]]=array[i];
////                }
////                else
////                newArray[pos[(pos.length-p--)+1][0]]=array[i];
////            }else if(array[i]<0){
////                if((neg.length-1)-1<0){
////                    newArray[neg[(neg.length-1)][0]]=array[i];
////                }
////                else
////                newArray[neg[(neg.length-1)-1][0]]=array[i];
////            }
////        }
////        System.out.println(Arrays.toString(newArray));
////        if((pos.length-p--+1)>pos.length-1){
////            newArray[[0]];
////        }
////        newArray[pos[pos.length-p--+1][0]]=pos[pos.length-p--][1];
//    }
//}


import java.util.Arrays;
import java.util.stream.IntStream;

public class Shift {

    private static int[] generate(int size) {
        return IntStream.range(0, size)
                .map(n -> (int)(Math.random()*200-100))
                .toArray();
    }

    private static int[] rotate_right(int[] origin) {
        int cnt = 0;
        int[] pos = new int[origin.length];
        for (int i = 0; i < origin.length; i++) {
            if (origin[i]>0) {
                pos[cnt++] = i;
            }
        }
        if (cnt<2) return origin;
        int[] rotated = origin.clone();  // COPY ALL ELEMENTS FROM ORIGIN ANG !!!!!!!! CHANGE ONLY NECESSARY ONES;
        for (int i = 0; i < cnt-1; i++) {
            int curr_pos = pos[i];  // IT GIVES US ONLY INDEXES OF THE ORIGINAL; I MEAN  |==> POS <==|
            int next_pos = pos[i+1];
            rotated[next_pos] = origin[curr_pos];
        }
        rotated[pos[0]]=origin[pos[cnt-1]];
        return rotated;
    }
    public static int[] rotate_left(int[] origin){
        int cnt = 0;
        int[] pos = new int[origin.length];
        for (int i = 0; i < origin.length; i++) {
            if (origin[i]<0) {
                pos[cnt++] = i;
            }
        }
        if(cnt<2)return origin;
        int[] rotated=origin.clone();
        for(int i=cnt-1;i>0;i--){
            int current=pos[i];
            int previous=pos[i-1];
            rotated[previous]=origin[current];
        }
        rotated[pos[cnt-1]]=origin[pos[0]];
        return rotated;
    }

    public static void main(String[] args) {
        int[] origin = generate(10);
        int[] shifted_right = rotate_right(origin);
        int[] shifted_left=rotate_left(origin);
        System.out.printf("Original: %s\n", Arrays.toString(origin));
        System.out.printf("Rotated right : %s\n", Arrays.toString(shifted_right));
        System.out.printf("Rotated left : %s\n", Arrays.toString(shifted_left));
    }

}
