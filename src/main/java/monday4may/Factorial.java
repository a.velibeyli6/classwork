package monday4may;

public class Factorial {
    static void extraLongFactorials(int n) {

        System.out.println(doFactorial(n));
    }

    private static long doFactorial(int n) {
        if (n == 1 || n == 0) return 1;
        return n * doFactorial(n - 1);
    }


}
