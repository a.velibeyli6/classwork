package monday4may;

import java.util.ArrayList;
import java.util.List;

public class FindingDigits {
    static int findDigits(int n) {
        int k = n;
        int count = 0;
        List<Integer> digits = new ArrayList<>();
        while (n > 0) {
            digits.add(n % 10);
            n /= 10;
        }
        for (int i : digits) {
            if (i != 0 && k % i == 0) count++;
        }
        return count;

    }
}
