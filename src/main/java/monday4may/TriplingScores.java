package monday4may;

import java.util.ArrayList;
import java.util.List;

public class TriplingScores {
    static List<Integer> compareTriplets(List<Integer> a, List<Integer> b) {
        int one = 0, two = 0;
        for (int i = 0; i < a.size(); i++) {
            if (a.get(i) > b.get(i)) one++;
            else if (a.get(i) < b.get(i)) two++;
        }
        List<Integer> result = new ArrayList<>();
        result.add(one);
        result.add(two);
        return result;

    }
}
