package friday1may;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SQLnewDB {
    private static final String URL = "jdbc:postgresql://localhost:5432/Customers";
    private static final String NAME = "postgres";
    private static final String PASSWORD = "secret";

    public static void main(String[] args) throws FileNotFoundException, SQLException {
        List<List<String>> datas = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(new File("custnames.txt")))) {
            List<String> custnames = br.lines().map(String::valueOf).collect(Collectors.toList());
            datas.add(custnames);
        } catch (Exception ex) {
            throw new RuntimeException("blah blah");
        }

        try (BufferedReader br = new BufferedReader(new FileReader(new File("names.txt")))) {
            List<String> custnames = br.lines().map(String::valueOf).collect(Collectors.toList());
            datas.add(custnames);
        } catch (Exception ex) {
            throw new RuntimeException("blah blah");
        }

        try (BufferedReader br = new BufferedReader(new FileReader(new File("address.txt")))) {
            List<String> custnames = br.lines().map(String::valueOf).collect(Collectors.toList());
            datas.add(custnames);
        } catch (Exception ex) {
            throw new RuntimeException("blah blah");
        }

        try (BufferedReader br = new BufferedReader(new FileReader(new File("city.txt")))) {
            List<String> custnames = br.lines().map(String::valueOf).collect(Collectors.toList());
            datas.add(custnames);
        } catch (Exception ex) {
            throw new RuntimeException("blah blah");
        }

        try (BufferedReader br = new BufferedReader(new FileReader(new File("postalcode.txt")))) {
            List<String> custnames = br.lines().map(String::valueOf).collect(Collectors.toList());
            datas.add(custnames);
        } catch (Exception ex) {
            throw new RuntimeException("blah blah");
        }

        try (BufferedReader br = new BufferedReader(new FileReader(new File("country.txt")))) {
            List<String> custnames = br.lines().map(String::valueOf).collect(Collectors.toList());
            datas.add(custnames);
        } catch (Exception ex) {
            throw new RuntimeException("blah blah");
        }

        Connection conn = DriverManager.getConnection(URL, NAME, PASSWORD);
        String sql = "insert into Customers (id,customername,contactname,address,city,postalcode,country) values (default,?,?,?,?,?,?)";
        PreparedStatement pr = conn.prepareStatement(sql);
//        for (int i = 0; i < datas.get(0).size(); i++) {


            pr.setString(1, datas.get(0).get(0));
            pr.setString(2, datas.get(1).get(0));
            pr.setString(3, datas.get(2).get(0));
            pr.setString(4, datas.get(3).get(0));
            pr.setString(5, datas.get(4).get(0));
            pr.setString(6, datas.get(5).get(0));
            pr.executeUpdate();




        System.out.println(datas);


    }


}
