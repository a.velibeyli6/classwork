package wednesday29april;

import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Stream;

public class CountingValleys {
    private final int U = 1;
    private final int D = -1;

    public static void main(String[] args) {


    }

    static int countingValleys(int n, String s) {
        char[] chars = s.toCharArray();
        int count = 0;
        int level = 0;

        for (char aChar : chars) {
            if (aChar == 'U') {
                count++;
            } else if (aChar == 'D') {
                if (count == 0) {
                    level++;
                }
                count--;
            }
        }

        return level;
    }
}
