package tuesday3march;

import java.util.Optional;

public class Main {
    public static Optional<Integer> strToInt(String raw){
        try{
            return Optional.of(Integer.parseInt(raw));
        }catch(NumberFormatException  ex){
            return Optional.empty();
        }
    }

    public static void main(String[] args) {
        Optional<Integer> value1= strToInt("123");
        Optional<Integer> value2= strToInt("123a");
        System.out.println(value1);
        System.out.println(value2);

    }
}
