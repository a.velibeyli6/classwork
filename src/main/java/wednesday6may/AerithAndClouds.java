package wednesday6may;

public class AerithAndClouds {

    public static void main(String[] args) {
        System.out.println(jumpingOnClouds(new int[]{0, 0, 1, 0, 0, 1, 1, 0}, 2));
    }

    // Complete the jumpingOnClouds function below.
    // 8    2
    // 0 0 1 0 0 1 1 0
    static int jumpingOnClouds(int[] c, int k) {
        int E = 100;
        int n = c.length;

//        boolean IsAtHome = false;
//        int a = 0;
        int curr;
//        while (!IsAtHome) {
//            a = a + k;
//            curr = c[(a) % n];
//            E--;
//            if (curr == 1) E -= 2;
//            if (a % n == 0) IsAtHome = true;
//        }
        boolean isDone = false;
        for (int i = k; !isDone; i += k) {
            curr = c[(i) % n];
            E--;
            if (curr == 1) E -= 2;
            if (i % n == 0) isDone = true;
        }

        return E;
    }
}
