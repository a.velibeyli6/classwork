package wednesday6may;

import java.util.Arrays;
import java.util.Comparator;


public class ClimbingOnTheLeaderBoard {

    static int checkIt(Integer[] scores, int x) {
        int idx = Arrays.binarySearch(scores, x, new Comparator<Integer>() {
            @Override
            public int compare(Integer integer, Integer t1) {
                return t1 - integer;
            }
        });
        if (idx < 0) return Math.abs(idx);
        return idx + 1;
    }

    static int[] climbingLeaderboard(int[] scores, int[] alice) {
        Integer[] scoresList = Arrays.stream(scores).boxed().distinct().toArray(Integer[]::new);
        return Arrays.stream(alice).map(x -> checkIt((scoresList), x)).toArray();
    }
}
